json.extract! tweet, :id, :hashtag, :automode, :created_at, :updated_at
json.url tweet_url(tweet, format: :json)
