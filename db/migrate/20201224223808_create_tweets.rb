class CreateTweets < ActiveRecord::Migration[6.1]
  def change
    create_table :tweets do |t|
      t.string :hashtag
      t.boolean :automode

      t.timestamps
    end
  end
end
