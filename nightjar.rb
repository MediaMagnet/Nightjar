#!/usr/bin/ruby
#frozen_string_literal = true

require 'twitter'
require 'toml'


conf = TOML.load_file("config.toml")

client = Twitter::Streaming::Client.new do |config|
  config.consumer_key = conf['twitter']['consumer_key']
  config.consumer_secret = conf['twitter']['consumer_secret']
  config.access_token = conf['twitter']['access_token']
  config.access_token_secret = conf['twitter']['token_secret']
end

puts "Nightjar chirping on twitter. \n"

topics = ["testtweet", "tasgiving2020:w:"]
client.filter(track: topics.join(",")) do |object|
  puts "#{object.user.screen_name}: #{object.text}" if object.is_a?(Twitter::Tweet)
end
